#Walter Backend


## Description

## .env.development

### DATABASE CONNECTION
URI_MONGODB=mongodb://walter_user:secret@localhost:27017/walter_db

###JWT
JWT_SECRET=JWTP5d1&0MCEAz&poFt
EXPIRES_IN=12h

###API
APP_URL=https://backend.waltersoluciones.com
PORT=3000

####TWILIO:
TWILIO_ACCOUNT_SID=AC***
TWILIO_AUTH_TOKEN=****
TWILIO_PHONE_NUMBER=<twilio_phone_number>

## Installation

bash
$ npm install


## Running the app

bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod


## Test

bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov


## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
import { Module } from "@nestjs/common";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import { UserModule } from "../user/user.module";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { LocalStrategy } from "./strategies/local-strategy";
import { JwtStrategy } from "./strategies/jwt-strategy";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TwilioService } from "./twilio/twilio.service";
import { MongooseModule } from "@nestjs/mongoose";
import { USER } from "../common/models/models";
import { UserSchema } from "../user/schema/user.schema";

@Module({
  imports: [
    UserModule,
    PassportModule,
    MongooseModule.forFeatureAsync([
      {
        name: USER.name,
        useFactory: () => {
          return UserSchema;
        },
      },
    ]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>("JWT_SECRET"),
        signOptions: {
          expiresIn: configService.get<string>("EXPIRES_IN"),
          audience: configService.get<string>("APP_URL")
        }
      }),
      inject: [ConfigService]
    })
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy, TwilioService]
})
export class AuthModule {
}

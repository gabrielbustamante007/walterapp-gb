import { Body, Controller, Post, Req, UseGuards } from "@nestjs/common";
import { ApiOperation, ApiTags } from "@nestjs/swagger";
import { AuthService } from "./auth.service";
import { UserDTO } from "../user/dto/user.dto";
import { LocalAuthGuard } from "./guards/local-auth.guard";
import { RequestBodyObject } from "@nestjs/swagger/dist/interfaces/open-api-spec.interface";
import { TwilioService } from "./twilio/twilio.service";

@ApiTags("Authentication")
@Controller("api/v1/auth")
export class AuthController {
  constructor(private readonly authService: AuthService, private readonly twilioService: TwilioService) {
  }

  @UseGuards(LocalAuthGuard)
  @Post("signin")
  @ApiOperation({
    summary: "Login.",
    description: "Login.",
    requestBody: <RequestBodyObject>{
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              username: {
                type: "string",
                example: "user"
              },
              password: {
                type: "string",
                example: "password"
              }
            }
          }
        }
      }
    }
  })
  async signIn(@Req() req) {
    return await this.authService.signIn(req.user);
  }

  @Post("signup")
  async signUp(@Body() userDTO: UserDTO) {
    return await this.authService.signUp(userDTO);
  }

  @Post("generate-otp")
  @ApiOperation({
    summary: "Generate OTP.",
    description: "Generate OTP.",
    requestBody: <RequestBodyObject>{
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              phone_number: {
                type: "string",
                example: "+573001112233"
              }
            }
          }
        }
      }
    }
  })
  async generateOTP(@Req() req) {
    return await this.twilioService.saveOTP(req.body.phone_number);
  }

  @Post("validate-otp")
  @ApiOperation({
    summary: "Validate OTP.",
    description: "Validate OTP.",
    requestBody: <RequestBodyObject>{
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              phone_number: {
                type: "string",
                example: "+573001112233"
              },
              otp: {
                type: "number",
                example: "99999"
              }
            }
          }
        }
      }
    }
  })
  async validateOTP(@Req() req) {
    return await this.twilioService.validateOTP(req.body.phone_number, req.body.otp);
  }

}

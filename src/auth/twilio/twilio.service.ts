import { Injectable } from "@nestjs/common";
import { UserService } from "../../user/user.service";
import { Model } from "mongoose";
import { IOTPSuccess } from "../../common/interfaces/otp.interface";
import { randomInt } from "crypto";
import * as twilio from "twilio";
import { InjectModel } from "@nestjs/mongoose";
import { USER } from "../../common/models/models";
import { IUser } from "../../common/interfaces/user.interface";

@Injectable()
export class TwilioService {

  private twilioClient: twilio.Twilio;

  constructor(private readonly userService: UserService) {
    this.twilioClient = twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);
  }

  /**
   * Save OTP
   * @param phone
   */
  async saveOTP(phone: string): Promise<IOTPSuccess> {

    const user = await this.userService.findPhone(phone);
    if (user) {
      const otp = this.generateOTP();
      const otpExpiredAt = this.generateDuration();
      //Update OTP
      await this.userService.updateOTP(user._id.toString(), otp, otpExpiredAt);
      //Send OTP
      await this.sendOTP(phone, otp, otpExpiredAt);
      return <IOTPSuccess>{ success: true, message: `Walter OTP Code: ${otp}` };
    } else {
      return <IOTPSuccess>{ success: false, message: `Phone not found!` };
    }
  }


  /**
   * Generate OTP
   * @returns
   */
  generateOTP(): string {
    const codInt: number = randomInt(10000, 99999);
    return `${codInt}`;
  }

  /**
   * Generate Duration
   * return Date
   */
  generateDuration(): Date {
    const second = 1000;
    const now: Date = new Date();
    return new Date(now.getTime() + 60 * second);
  }

  /**
   * Send OTP
   * @param phone
   * @param otp
   * @param otpExpiredAt
   */
  async sendOTP(phone: string, otp: string, otpExpiredAt: Date) {
    console.log(`Walter OTP Code: ${otp}`);
    try {
      await this.twilioClient.messages
        .create({
          body: `Walter OTP Code: ${otp}`,
          from: process.env.TWILIO_PHONE_NUMBER,
          to: `${phone}`
        })
        .then((message) => console.log(message));
    } catch (err) {
      console.log(err);
    }
  }

  /**
   * Validate OTP
   * @param phone
   * @param otp
   */
  async validateOTP(phone: string, otp: number): Promise<IOTPSuccess> {

    const user = await this.userService.findPhoneAndOTP(phone, otp);
    if (user) {

      //Validate OTP
      if (Date.parse(user.otpExpiredAt.toISOString()) <= Date.now()) {
        return <IOTPSuccess>{ success: false, message: `Walter OTP expired` };
      }

      //Delete OTP
      await this.userService.deleteOTP(user._id.toString());
      //Return Message
      return <IOTPSuccess>{ success: true, message: `Walter OTP Valid` };


    } else {
      return <IOTPSuccess>{ success: false, message: `Data not found!` };
    }
  }


}

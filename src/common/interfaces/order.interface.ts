export interface IOrder extends Document {
  city: string;
  date: Date;
  declaredValue: number;
  distance: number;
  distanceTotal: number;
  estimateTime: string;
  userId: string;
  paymentType: string;
  phone: string;
  serviceValue: number;
  trackingStep: string;
}


export interface IUser extends Document {
  _id: string;
  name: string;
  username: string;
  email: string;
  password: string;
  phone: string;
  otp: string;
  otpExpiredAt: Date;
}

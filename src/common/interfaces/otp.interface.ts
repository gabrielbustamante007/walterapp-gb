export interface IOTPSuccess extends Document {
  success: boolean;
  message: string;
}
import { BaseExceptionFilter, NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { AllExceptionFilter } from "./common/filters/http-exception.filter";
import { TimeoutInterceptor } from "./common/interceptors/timeout.interceptor";
import { ArgumentsHost, Catch, ValidationPipe } from "@nestjs/common";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

@Catch()
export class AllExceptionsFilter extends BaseExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    super.catch(exception, host);
  }
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new AllExceptionFilter());
  app.useGlobalInterceptors(new TimeoutInterceptor());
  app.useGlobalPipes(new ValidationPipe());

  const options = new DocumentBuilder()
    .setTitle("Walter Backend")
    .setDescription("Walter App")
    .setVersion("1.0.0")
    .addBearerAuth(
      { type: "http", scheme: "bearer", bearerFormat: "JWT", description: "Authentication with tokens" },
      "bearerAuth"
    )
    .addSecurity("bearerAuth", {
      type: "http",
      scheme: "bearer",
      bearerFormat: "JWT",
      description: "Authentication with tokens"
    })
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup("/api/docs", app, document);

  await app.listen(process.env.PORT || 3000);
}

bootstrap();

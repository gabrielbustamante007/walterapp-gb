import { IsDate, IsEmail, IsNotEmpty, IsOptional, IsString } from "class-validator";
import { Exclude, Type } from "class-transformer";

export class UserDTO {

  @IsOptional()
  @Exclude()
  _id?: string;

  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @IsNotEmpty()
  @IsString()
  readonly username: string;

  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  @IsString()
  readonly password: string;

  @IsNotEmpty()
  @IsString()
  readonly phone: string;

  @IsOptional()
  @IsString()
  readonly otp: string;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  readonly otpExpiredAt: Date;
}

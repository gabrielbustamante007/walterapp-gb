import {
  Body,
  Controller,
  Delete,
  Get, HttpCode, HttpStatus,
  Param,
  Post,
  Put, UseGuards
} from "@nestjs/common";
import { UserDTO } from "./dto/user.dto";
import { UserService } from "./user.service";
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
  getSchemaPath
} from "@nestjs/swagger";
import { RequestBodyObject } from "@nestjs/swagger/dist/interfaces/open-api-spec.interface";
import { LocalAuthGuard } from "../auth/guards/local-auth.guard";
import { JwtAuthGuard } from "../auth/guards/jwt-auth.guard";

@ApiTags("users")
@Controller("api/v1/user")
export class UserController {
  constructor(private readonly userService: UserService) {
  }


  @Post()
  @ApiOperation({
    summary: "Create User.",
    description: "Create User.",
    requestBody: <RequestBodyObject>{
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              name: {
                type: "string",
                example: "John Doe"
              },
              username: {
                type: "string",
                example: "john_doe"
              },
              email: {
                type: "string",
                example: "john_doe@example.com"
              },
              password: {
                type: "string",
                example: "secret"
              },
              phone: {
                type: "string",
                example: "+573001112233"
              }
            }
          }
        }
      }
    }
  })
  store(@Body() userDTO: UserDTO) {
    return this.userService.store(userDTO);
  }

  /**
   * Get All Users.
   */
  @Get()
  @UseGuards(JwtAuthGuard)
  @HttpCode(HttpStatus.OK)
  @ApiBearerAuth("bearerAuth")
  @ApiUnauthorizedResponse({ description: "Unauthorized." })
  @ApiOkResponse({
    description: "Get All Users.",
    schema: { type: "array", items: { $ref: getSchemaPath(UserDTO) } }
  })
  @ApiOperation({
    summary: "Get All Users",
    description: "Get All Users."
  })
  getAll() {
    return this.userService.getAll();
  }

  @Get(":id")
  find(@Param("id") id: string) {
    return this.userService.find(id);
  }

  @Put(":id")
  update(@Param("id") id: string, @Body() userDTO: UserDTO) {
    return this.userService.update(id, userDTO);
  }

  @Delete(":id")
  destroy(@Param("id") id: string) {
    return this.userService.destroy(id);
  }
}

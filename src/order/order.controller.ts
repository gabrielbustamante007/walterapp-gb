import {
  Body,
  Controller,
  Delete,
  Get,
  Param, Patch,
  Post,
  Put, UseGuards
} from "@nestjs/common";
import { OrderDTO } from "./dto/order.dto";
import { OrderService } from "./order.service";
import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
  getSchemaPath
} from "@nestjs/swagger";
import { JwtAuthGuard } from "../auth/guards/jwt-auth.guard";
import { RequestBodyObject } from "@nestjs/swagger/dist/interfaces/open-api-spec.interface";
import * as Path from "path";
import { GeoLocationDTO } from "./dto/geo-location.dto";

@ApiTags("Orders")
@Controller("api/v1/order")
export class OrderController {
  constructor(private readonly orderService: OrderService) {
  }

  /**
   * Create Order
   * @param orderDTO
   */
  @UseGuards(JwtAuthGuard)
  @Post()
  @ApiOkResponse({ description: "Order Successful Created.", type: OrderDTO })
  @ApiUnauthorizedResponse({ description: "Unauthorized." })
  @ApiOperation({
    summary: "Do orders for authenticated users.",
    description: "Do orders for authenticated users."
  })
  store(@Body() orderDTO: OrderDTO) {
    return this.orderService.store(orderDTO);
  }

  /**
   * Get Orders
   * @param orderDTO
   */
  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiOkResponse({
    description: "Get Orders.",
    schema: { type: "array", items: { $ref: getSchemaPath(OrderDTO) } }
  })
  @ApiUnauthorizedResponse({ description: "Unauthorized." })
  @ApiOperation({
    summary: "Get Orders.",
    description: "Get Orders."
  })
  getAll() {
    return this.orderService.getAll();
  }

  /**
   * Get Order By ID
   * @param orderDTO
   */
  @UseGuards(JwtAuthGuard)
  @Get(":id")
  @ApiParam({ name: "id", description: "Order By", type: "string" })
  @ApiOkResponse({
    description: "Get Order By ID.",
    schema: { type: "array", items: { $ref: getSchemaPath(OrderDTO) } }
  })
  @ApiUnauthorizedResponse({ description: "Unauthorized." })
  @ApiOperation({
    summary: "Get Order By ID.",
    description: "Get Order By ID."
  })
  find(@Param("id") id: string) {
    return this.orderService.find(id);
  }

  /**
   * Update Order
   * @param orderDTO
   */
  @UseGuards(JwtAuthGuard)
  @Put(":id")
  @ApiParam({ name: "id", description: "Order By", type: "string" })
  @ApiOkResponse({
    description: "Order Updated.",
    type: OrderDTO
  })
  @ApiUnauthorizedResponse({ description: "Unauthorized." })
  @ApiNotFoundResponse({ description: "Order Not Found" })
  @ApiBadRequestResponse({ description: "Wrong values." })
  @ApiOperation({
    summary: "Update Order By ID.",
    description: "Update Order By ID."
  })
  update(@Param("id") id: string, @Body() orderDTO: OrderDTO) {
    return this.orderService.update(id, orderDTO);
  }

  /**
   * Delete Order
   * @param orderDTO
   */
  @UseGuards(JwtAuthGuard)
  @Delete(":id")
  @ApiParam({ name: "id", description: "Order By", type: "string" })
  @ApiOkResponse({
    description: "Order Deleted.",
    type: OrderDTO
  })
  @ApiUnauthorizedResponse({ description: "Unauthorized." })
  @ApiNotFoundResponse({ description: "Order Not Found" })
  @ApiOperation({
    summary: "Delete Order By ID.",
    description: "Delete Order By ID."
  })
  @Delete(":id")
  destroy(@Param("id") id: string) {
    return this.orderService.destroy(id);
  }

  /**
   * Tracking Order
   * @param orderDTO
   */
  @UseGuards(JwtAuthGuard)
  @Patch(":id")
  @ApiParam({ name: "id", description: "Order By", type: "string" })
  @ApiOkResponse({
    description: "Order Deleted.",
    type: OrderDTO
  })
  @ApiUnauthorizedResponse({ description: "Unauthorized." })
  @ApiNotFoundResponse({ description: "Order Not Found" })
  @ApiOperation({
    summary: "Delete Order By ID.",
    description: "Delete Order By ID."
  })
  updateField(@Param("id") id: string, @Body() orderDTO: OrderDTO) {
    return this.orderService.update(id, orderDTO);
  }


  /**
   * Order Distance
   */
  @UseGuards(JwtAuthGuard)
  @Post("distance")
  @ApiOkResponse({
    description: "Order Distance."
  })
  @ApiUnauthorizedResponse({ description: "Unauthorized." })
  calculateDistance(@Body() geoLocationDTO: GeoLocationDTO) {
    return this.orderService.calculateDistance(geoLocationDTO);
  }


}

import { HttpStatus, Injectable } from "@nestjs/common";
import { OrderDTO } from "./dto/order.dto";
import { ORDER } from "../common/models/models";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IOrder } from "src/common/interfaces/order.interface";
import { GeoLocationDTO } from "./dto/geo-location.dto";
import { IPosition } from "../common/interfaces/position.interface";

@Injectable()
export class OrderService {

  private readonly costService = 4000;

  constructor(@InjectModel(ORDER.name) private readonly model: Model<IOrder>) {
  }

  /**
   * Create Order
   * @param orderDTO
   */
  async store(orderDTO: OrderDTO): Promise<IOrder> {
    const newOrder = new this.model({ ...orderDTO });
    return await newOrder.save();
  }

  /**
   * Get All Orders
   */
  async getAll(): Promise<IOrder[]> {
    return this.model.find();
  }

  /**
   * Find Order
   * @param id
   */
  async find(id: string): Promise<IOrder> {
    return this.model.findById(id);
  }

  /**
   * Update Order
   * @param id
   * @param orderDTO
   */
  async update(id: string, orderDTO: OrderDTO): Promise<IOrder> {
    const order = { ...orderDTO };
    return this.model.findByIdAndUpdate(id, order, { new: true });
  }

  /**
   * Destroy Order
   * @param id
   */
  async destroy(id: string) {
    await this.model.findByIdAndDelete(id);
    return {
      status: HttpStatus.OK,
      message: "Deleted"
    };
  }

  calculateDistance(geoLocationDTO: GeoLocationDTO) {
    const { locations, roundTrip } = geoLocationDTO;
    const distances = [];
    for (let i = 0; i < locations.length - 1; i++) {
      distances.push(this.getDistanceFromLatLonInKm(locations[i], locations[i + 1]));
    }

    //RoundTrip
    if (roundTrip) {
      distances.push(this.getDistanceFromLatLonInKm(locations[0], locations[locations.length - 1]));
    }

    const distance = Math.ceil(distances.reduce((acc, cur) => acc + cur));
    if (distance <= 1) return { price: this.costService, distance };

    return { price: this.costService * distance, distance };

  }

  getDistanceFromLatLonInKm(origin: IPosition, destination: IPosition) {

    const { lat: lat1, lng: lon1 } = origin;
    const { lat: lat2, lng: lon2 } = destination;

    const R = 6371;
    const dLat = this.deg2rad(lat2 - lat1);
    const dLon = this.deg2rad(lon2 - lon1);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180);
  }

}

import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import {
  ArrayMinSize,
  IsArray, IsBoolean,
  IsNotEmpty,
  IsNumber,
  ValidateNested
} from "class-validator";

export class Locations {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  lat: number;

  @IsNotEmpty()
  @IsNumber()
  lng: number;
}

export class GeoLocationDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(2)
  @Type(() => Locations)
  readonly locations: Locations[];

  @IsBoolean()
  readonly roundTrip: boolean = false;
}

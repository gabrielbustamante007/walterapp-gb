import { IsNotEmpty, IsString, IsNumber, IsDate, MaxLength, Min, IsIn, MinLength, IsOptional } from "class-validator";
import { Type } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

export class OrderDTO {

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  readonly city: string = "MEDELLIN";

  @IsNotEmpty()
  @IsOptional()
  @Type(() => Date)
  @IsDate()
  readonly date: Date = new Date();

  @IsNotEmpty()
  @IsOptional()
  @IsNumber()
  @Min(0)
  @ApiProperty({
    minimum: 0
  })
  readonly declaredValue: number = 0;

  @IsNotEmpty()
  @IsOptional()
  @IsNumber()
  @Min(1)
  @ApiProperty({
    minimum: 1
  })
  readonly distance: number;

  @IsNotEmpty()
  @IsOptional()
  @IsNumber()
  @Min(1)
  @ApiProperty({
    minimum: 1
  })
  readonly distanceTotal: number;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  readonly estimateTime: string;

  @MaxLength(45)
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    maxLength: 45
  })
  readonly userId: string;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @IsIn(["CARD", "CASH"])
  @ApiProperty({
    enum: ["CARD", "CASH"]
  })
  readonly paymentType: string;

  @IsOptional()
  @MaxLength(13)
  @MinLength(13)
  @ApiProperty({
    minLength: 13,
    maxLength: 13,
    pattern: "^+573[0-9]{9}$",
    description: "Prefix =57"
  })
  readonly phone: string;

  @IsOptional()
  @IsNumber()
  @IsNotEmpty()
  @Min(0)
  @ApiProperty({
    minimum: 0,
    default: 0
  })
  readonly serviceValue: number = 0;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @IsIn(["IDLE", "INITIAL", "ARRIVAL", "DELIVERY", "EVALUATION"])
  readonly trackingStep: string;
}

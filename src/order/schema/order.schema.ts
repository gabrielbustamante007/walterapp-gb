import * as mongoose from "mongoose";

export const OrderSchema = new mongoose.Schema(
  {
    city: { type: String, required: true },
    date: { type: Date, required: true },
    declareValue: { type: Number, required: true },
    distance: { type: Number, required: true },
    distanceTotal: { type: Number, required: true },
    estimateTime: { type: String, required: true },
    userId: { type: String, required: true },
    paymentType: { type: String, required: true },
    phone: { type: String, required: true },
    serviceValue: { type: Number, required: true },
    trackingStep: { type: String, required: true }
  },
  { timestamps: true }
);
